import React from 'react';
import PropTypes from 'prop-types'; // ES6

export default class  InputChange extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      inputValue : '',
      name:'sumit'
    }
  };
   
  handleChangeInput = evt => { 
    this.setState({inputValue:evt.target.value})
    console.log(evt.target.value)
  }
  handleClick = () =>{
    this.setState({
      name:'varnit'
    },()=>{console.log('set state done')})
  } 

  render(){
    return(
      <div>
        <h1>{this.state.name}</h1>
        <button type="button" name="name_change_button" onClick={this.handleClick}>Change Name</button>
       <input type = "text" value = {this.state.inputValue}
        onChange = { evt => this.handleChangeInput(evt)}/>
         {this.state.inputValue}
        </div>
    )
  }
  
} 
