import React,{Component} from 'react';
import PropsExample from './PropsComponent';
import Clock from './ClockComponent';
import InputChange from './InputchangeComponent'
import PropTypesExample from './PropTypesExample';

function App() {
  return (
    <div>
      <PropTypesExample name="varnit"/>
      <InputChange/>
	    <h1> app component </h1>
      <Clock/>
      <Converter/>
      <PropsExample 
        title="PROPS EXAMPLE"
        description="A Props Description" 
        />
    </div>
  );
}

  class Converter extends Component{
    constructor(props){
      super(props)
      this.state={ currency: '@' }
    }

    handleChangeCurrency = event => {
      this.setState(
        { 
          currency:this.state.currency  === '@' ? '$' : '@'
        }
      ) 
    }

    render(){
      return(
        <div>
          <Display currency={this.state.currency}/>
          <CurrencySwitcher 
             currency={this.state.currency}
             handleChangeCurrency={this.handleChangeCurrency}
          />
        </div>
      )
    }
  }

  const CurrencySwitcher = props => {
    return(
      <button onClick={props.handleChangeCurrency}>
        Current currency is {props.currency}.change it!
      </button>
    )
  }

  const Display = props => {
     return <p> 
       Current currency is {props.currency}.</p> 
  }





export default App;
 