import React ,{Component} from 'react';


const PropsFunc = props => {
  return(
    <div>
      <h1>{props.title}</h1>
      <h1>{props.description}</h1>
      </div>
  )
}

  export default class PropsExample extends Component{
  render(){
    return(
      <div>
        <h1>{this.props.title}</h1>
        <h1>{this.props.description}</h1>
        </div>
    )
  }
}