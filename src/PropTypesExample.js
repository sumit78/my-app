import PropTypes from 'prop-types';
import React, {Component} from 'react';

class PropTypesExample extends Component{
  render(){
    return(
      <div>
        proptypes example 
        {
          this.props.name
        }
        {
          this.props.age
        }
      </div>
    )
  }

}
PropTypesExample.propTypes ={
  name:PropTypes.string.isRequired,
  age:PropTypes.number
}
PropTypesExample.defaultProps ={
  age:20,
}
export default PropTypesExample;